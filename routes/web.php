<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['as' => 'holiday.'], function () {
    Route::get('/', 'VerifyHolidayController@index')->name('index');
    Route::post('/verify', 'VerifyHolidayController@verify')->name('verify');
});
Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/verify', 'VerifyController@getVerify')->name('getVerify');
//Route::post('/verify', 'VerifyController@postVerify')->name('postVerify');
//Route::post('/refresh-verify', 'VerifyController@refreshVerify')->name('refreshVerify');
