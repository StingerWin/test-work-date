<?php


namespace App\Services;


use Carbon\Carbon;

class HolidayServices
{
    protected static function holidayArr()
    {
        $dates_holiday_arr = [
            0 => [
                'date' => '01-01',
                'month' => '',
                'week' => '',
                'day' => '',
                'name_holiday' => 'New Year.',
            ],
            1 => [
                'date' => '01-07',
                'month' => '',
                'week' => '',
                'day' => '',
                'name_holiday' => 'Christmas.',
            ],
            2 => [
                'date' => '',
                'month' => '1',
                'week' => '3',
                'day' => '1',
                'name_holiday' => 'Monday of the 3rd week of January.',
            ],
            3 => [
                'date' => '',
                'month' => '3',
                'week' => '5',
                'day' => '1',
                'name_holiday' => 'Monday of the last week of March.',
            ],
            4 => [
                'date' => '',
                'month' => '11',
                'week' => '4',
                'day' => '4',
                'name_holiday' => 'Thursday of the 4th week of November.',
            ],
            5 => [
                'date' => '05-01',
                'month' => '',
                'week' => '',
                'day' => '',
                'name_holiday' => 'From 1st of May till 7th of May.',
            ],
            6 => [
                'date' => '05-02',
                'month' => '',
                'week' => '',
                'day' => '',
                'name_holiday' => 'From 1st of May till 7th of May.',
            ],
            7 => [
                'date' => '05-03',
                'month' => '',
                'week' => '',
                'day' => '',
                'name_holiday' => 'From 1st of May till 7th of May.',
            ],
            8 => [
                'date' => '05-04',
                'month' => '',
                'week' => '',
                'day' => '',
                'name_holiday' => 'From 1st of May till 7th of May.',
            ],
            9 => [
                'date' => '05-05',
                'month' => '',
                'week' => '',
                'day' => '',
                'name_holiday' => 'From 1st of May till 7th of May.',
            ],
            10 => [
                'date' => '05-06',
                'month' => '',
                'week' => '',
                'day' => '',
                'name_holiday' => 'From 1st of May till 7th of May.',
            ],
            11 => [
                'date' => '05-07',
                'month' => '',
                'week' => '',
                'day' => '',
                'name_holiday' => 'From 1st of May till 7th of May.',
            ],
        ];

        return $dates_holiday_arr;
    }

    public static function verifyHoliday($validated)
    {
        $date = Carbon::parse($validated['date']);
        $dateMD = $date->format('m-d');
        $dateDay = $date->dayOfWeek;
        $dateWeek = $date->weekOfMonth;
        $dateMonth = $date->month;

        $text = '';
        foreach (self::holidayArr() as $holy) {
            if (!empty(($holy['date']))) {
                if ($holy['date'] == $dateMD) {
                    $text .= self::holidaySatOrSun($text, $date, $holy);
                }
            } else {
                if ($holy['month'] == $dateMonth and $holy['week'] == $dateWeek and $holy['day'] == $dateDay) {
                    $text .= self::holidaySatOrSun($text, $date, $holy);
                }
            }
        }
        return $text;
    }

    protected static function holidaySatOrSun($text, $date, $holy)
    {
        $text .= $holy['name_holiday'];
        switch ($date->dayOfWeek) {
            case (6):
                $holiday = $date->addDays('+2')->format('d.m.Y');
                $text .= ' Holiday - ' . $holiday;
                break;
            case (0):
                $holiday = $date->addDays('+1')->format('d.m.Y');
                $text .= ' Holiday - ' . $holiday;
                break;
        }
        return $text;
    }
}
