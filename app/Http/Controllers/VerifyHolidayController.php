<?php

namespace App\Http\Controllers;

use App\Http\Requests\VerifyHolidayRequest;
use App\Services\HolidayServices;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerifyHolidayController extends Controller
{
    public function index()
    {
        return view('holiday');
    }

    public function verify(VerifyHolidayRequest $request)
    {
        $validated = $request->validated();

        return redirect()->route('holiday.index')->withText(HolidayServices::verifyHoliday($validated))->withDate($validated['date']);
    }
}
